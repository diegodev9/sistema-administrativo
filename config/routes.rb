# frozen_string_literal: true

# == Route Map
#

Rails.application.routes.draw do
  devise_for :users

  root to: 'home#index'

  resources :categories, except: [:show]
  resources :clients, except: [:show]
  resources :suppliers, except: [:show]
  resources :products
  resources :sales
  resources :warehouses
  resources :profiles, only: %w[show edit update]

  get 'buscar_producto/:termino', to: 'products#buscador'
  get '/buscador_clientes/:termino', to: 'clients#buscador'
  get '/buscador_proveedores/:termino', to: 'suppliers#buscador'
  post 'add_item_venta', to: 'sales#add_item'
  post 'add_cliente_venta', to: 'sales#add_client'
  post 'add_item_almacen', to: 'warehouses#add_item'
  post 'add_proveedor_entrada', to: 'warehouses#add_proveedor'
  post '/search', to: 'search#results'
  get '/search', to: 'home#index'

  #match '*unmatched', to: 'application#not_found_method', via: %i[get post]

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
