# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Application Routes', type: :routing do
  # root to: 'home#index'
  describe 'root path' do
    it { expect(get('/')).to route_to('home#index') }
  end

  # resources :categories, except: [:show]
  describe 'categories resources' do
    let(:new_category) { create(:category) }

    it { expect(get('/categories/')).to be_routable }
    it { expect(get("/categories/#{new_category.id}")).not_to be_routable }
    it { expect(patch("/categories/#{new_category.id}")).to be_routable }
    it { expect(delete("/categories/#{new_category.id}")).to be_routable }
  end

  # resources :clients, except: [:show]
  describe 'clients resources' do
    let(:new_client) { create(:client) }

    it { expect(get('/clients/')).to be_routable }
    it { expect(get("/clients/#{new_client.id}")).not_to be_routable }
    it { expect(patch("/clients/#{new_client.id}")).to be_routable }
    it { expect(delete("/clients/#{new_client.id}")).to be_routable }
  end

  # resources :suppliers, except: [:show]
  describe 'suppliers resources' do
    let(:new_supplier) { create(:supplier) }

    it { expect(get('/suppliers/')).to be_routable }
    it { expect(get("/suppliers/#{new_supplier.id}")).not_to be_routable }
    it { expect(patch("/suppliers/#{new_supplier.id}")).to be_routable }
    it { expect(delete("/suppliers/#{new_supplier.id}")).to be_routable }
  end

  # resources :products, except: [:show]
  describe 'products resources' do
    let(:new_product) { create(:product) }

    it { expect(get('/products/')).to be_routable }
    it { expect(get("/products/#{new_product.id}")).not_to be_routable }
    it { expect(patch("/products/#{new_product.id}")).to be_routable }
    it { expect(delete("/products/#{new_product.id}")).to be_routable }
  end

  # resources :sales
  describe 'sales resources' do
    # let(:new_sale) { create(:sale, sale_details: [FactoryBot.build(:sale_detail, sale_id: new_sale.id)]) }
    let(:new_sale) { create(:sale) }

    it { expect(get('/sales/')).to be_routable }
    it { expect(get("/sales/#{new_sale.id}")).to be_routable }
    it { expect(patch("/sales/#{new_sale.id}")).to be_routable }
    it { expect(delete("/sales/#{new_sale.id}")).to be_routable }
  end

  # resources :warehouses
  describe 'warehouses resources' do
    let(:new_product) { create(:product) }
    let(:new_warehouse_record) { create(:warehouse_record, product_id: new_product.id) }

    it { expect(get('/warehouses/')).to be_routable }
    it { expect(get("/warehouses/#{new_warehouse_record.id}")).to be_routable }
    it { expect(patch("/warehouses/#{new_warehouse_record.id}")).to be_routable }
    it { expect(delete("/warehouses/#{new_warehouse_record.id}")).to be_routable }
  end

  # resources :profiles, only: %w[show edit update]
  describe 'profiles resources' do
    let(:new_user) { create(:user) }
    let(:profile_id) { new_user.profile.id }

    it { expect(get('/profiles/')).not_to be_routable }
    it { expect(get("/profiles/#{profile_id}")).to be_routable }
    it { expect(patch("/profiles/#{profile_id}")).to be_routable }
    it { expect(delete("/profiles/#{profile_id}")).not_to be_routable }
  end


  # get 'buscar_producto/:termino', to: 'products#buscador'
  # get '/buscador_clientes/:termino', to: 'clients#buscador'
  # get '/buscador_proveedores/:termino', to: 'suppliers#buscador'
  # post 'add_item_venta', to: 'sales#add_item'
  # post 'add_cliente_venta', to: 'sales#add_client'
  # post 'add_item_almacen', to: 'warehouses#add_item'
  # post 'add_proveedor_entrada', to: 'warehouses#add_proveedor'
  describe 'other routes' do
    it { expect(get('buscar_producto/1')).to route_to('products#buscador', termino: '1') }
    it { expect(get('buscador_clientes/cliente1')).to route_to('clients#buscador', termino: 'cliente1') }
    it { expect(get('buscador_proveedores/proveedor1')).to route_to('suppliers#buscador', termino: 'proveedor1') }
    it { expect(post('add_item_venta')).to route_to('sales#add_item') }
    it { expect(post('add_cliente_venta')).to route_to('sales#add_client') }
    it { expect(post('add_item_almacen')).to route_to('warehouses#add_item') }
    it { expect(post('add_proveedor_entrada')).to route_to('warehouses#add_proveedor') }
  end
end
