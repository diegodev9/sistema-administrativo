# frozen_string_literal: true

# == Schema Information
#
# Table name: categories
#
#  id          :bigint           not null, primary key
#  nombre      :string
#  descripcion :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

FactoryBot.define do
  factory :category do
    nombre { Faker::Name.name }
    descripcion { 'Descripcion de la categoría' }
  end
end