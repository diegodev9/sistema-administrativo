# frozen_string_literal: true

# == Schema Information
#
# Table name: warehouse_records
#
#  id          :bigint           not null, primary key
#  cantidad    :integer
#  user_id     :integer
#  supplier_id :bigint
#  products_id :bigint
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

FactoryBot.define do
  factory :warehouse_record do
    cantidad { rand(1..10) }
    user
    supplier
    product
  end
end
