# frozen_string_literal: true

# == Schema Information
#
# Table name: suppliers
#
#  id         :bigint           not null, primary key
#  nombre     :string
#  direccion  :string
#  telefono   :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryBot.define do
  factory :supplier do
    nombre { Faker::Commerce.vendor }
    direccion { Faker::Address.full_address }
    telefono { Faker::PhoneNumber.phone_number_with_country_code }
  end
end
