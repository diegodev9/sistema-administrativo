# frozen_string_literal: true

# == Schema Information
#
# Table name: sale_details
#
#  id         :bigint           not null, primary key
#  cantidad   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  product_id :bigint           not null
#  sale_id    :bigint           not null

FactoryBot.define do
  factory :sale_detail do
    cantidad { rand(1..10) }
    product
    sale
  end
end
