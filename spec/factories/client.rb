# frozen_string_literal: true

# == Schema Information
#
# Table name: clients
#
#  id         :bigint           not null, primary key
#  direccion  :string
#  nombre     :string
#  telefono   :string
#  created_at :datetime         not null
#  updated_at :datetime         not null

FactoryBot.define do
  factory :client do
    direccion { Faker::Address.full_address }
    nombre { Faker::Name.name }
    telefono { Faker::PhoneNumber.cell_phone }
  end
end