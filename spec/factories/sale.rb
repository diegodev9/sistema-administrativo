# frozen_string_literal: true

# == Schema Information
#
#  id         :bigint           not null, primary key
#  importe    :decimal(, )
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  client_id  :bigint
#  user_id    :integer

FactoryBot.define do
  factory :sale do
    importe { 0 }
    client
    user
  end
end
