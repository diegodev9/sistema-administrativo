# frozen_string_literal: true

# == Schema Information
#
# Table name: products
#
#  id          :bigint           not null, primary key
#  descripcion :string
#  existencia  :integer
#  nombre      :string
#  precio      :decimal(, )
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  category_id :bigint           not null
#  supplier_id :bigint           not null
#
# Indexes
#
#  index_products_on_category_id  (category_id)
#  index_products_on_supplier_id  (supplier_id)
#
# Foreign Keys
#
#  fk_rails_...  (category_id => categories.id)
#  fk_rails_...  (supplier_id => suppliers.id)
#

FactoryBot.define do
  factory :product do
    nombre { Faker::Commerce.product_name }
    descripcion { 'descripcion de producto' }
    existencia { 100 }
    precio { Faker::Commerce.price }
    category
    supplier
  end
end
