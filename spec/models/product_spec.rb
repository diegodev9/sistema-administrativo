# frozen_string_literal: true

# == Schema Information
#
# Table name: products
#
#  id          :bigint           not null, primary key
#  descripcion :string
#  existencia  :integer
#  nombre      :string
#  precio      :decimal(, )
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  category_id :bigint           not null
#  supplier_id :bigint           not null
#
# Indexes
#
#  index_products_on_category_id  (category_id)
#  index_products_on_supplier_id  (supplier_id)
#
# Foreign Keys
#
#  fk_rails_...  (category_id => categories.id)
#  fk_rails_...  (supplier_id => suppliers.id)
#
require 'rails_helper'

RSpec.describe Product, type: :model do
  let(:new_product) { create(:product, nombre: 'producto1') }
  let(:new_product2) { create(:product, nombre: 'producto2') }

  before do
    new_product
    new_product2
  end

  it { is_expected.to belong_to(:category) }
  it { is_expected.to belong_to(:supplier) }
  it { is_expected.to have_many(:warehouse_records).dependent(:destroy) }
  it { is_expected.to have_one_attached(:image) }

  context 'when user wants to search a product' do
    it 'finds the product' do
      termino = 'producto1'

      expect(described_class.buscador(termino).count).to be >= 1
    end
  end

  context 'when user inputs half of the products name' do
    it 'finds the product' do
      termino = 'prod'

      expect(described_class.buscador(termino).count).to be >= 1
    end
  end
end
