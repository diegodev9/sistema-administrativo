# frozen_string_literal: true

# == Schema Information
#
# Table name: suppliers
#
#  id         :bigint           not null, primary key
#  nombre     :string
#  direccion  :string
#  telefono   :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
require 'rails_helper'

RSpec.describe Supplier, type: :model do
  let(:new_supplier) { create(:supplier, nombre: 'proveedor1') }
  let(:new_supplier2) { create(:supplier, nombre: 'proveedor2') }

  before do
    new_supplier
    new_supplier2
  end

  it { is_expected.to have_many(:warehouse_records).dependent(:destroy) }
  it { is_expected.to have_many(:products).dependent(:destroy) }

  context 'when user wants to search a supplier' do
    it 'finds the supplier' do
      termino = 'proveedor1'

      expect(described_class.buscador(termino).count).to be >= 1
    end
  end

  context 'when user inputs half of the suppliers name' do
    it 'finds the supplier' do
      termino = 'prov'

      expect(described_class.buscador(termino).count).to be >= 1
    end
  end
end
