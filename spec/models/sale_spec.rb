# frozen_string_literal: true

# == Schema Information
#
# Table name: sales
#
#  id         :bigint           not null, primary key
#  importe    :decimal(, )
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  client_id  :bigint
#  user_id    :integer
#
# Indexes
#
#  index_sales_on_client_id  (client_id)
#
# Foreign Keys
#
#  fk_rails_...  (client_id => clients.id)
#
require 'rails_helper'

RSpec.describe Sale, type: :model do
  it { is_expected.to belong_to(:client).optional(:true) }
  it { is_expected.to belong_to(:user) }
  it { is_expected.to have_many(:sale_details).dependent(:destroy) }
end
