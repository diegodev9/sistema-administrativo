# frozen_string_literal: true

# == Schema Information
#
# Table name: warehouse_records
#
#  id          :bigint           not null, primary key
#  cantidad    :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  product_id  :bigint
#  supplier_id :bigint
#  user_id     :integer
#
# Indexes
#
#  index_warehouse_records_on_product_id   (product_id)
#  index_warehouse_records_on_supplier_id  (supplier_id)
#
# Foreign Keys
#
#  fk_rails_...  (product_id => products.id)
#  fk_rails_...  (supplier_id => suppliers.id)
#
require 'rails_helper'

RSpec.describe WarehouseRecord, type: :model do
  it { is_expected.to belong_to(:supplier).optional(:true) }
  it { is_expected.to belong_to(:user) }
  it { is_expected.to have_many(:warehouse_details).dependent(:destroy) }
end
