# frozen_string_literal: true

# == Schema Information
#
# Table name: clients
#
#  id         :bigint           not null, primary key
#  nombre     :string
#  direccion  :string
#  telefono   :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
require 'rails_helper'

RSpec.describe Client, type: :model do
  let(:new_client) { create(:client, nombre: 'cliente1') }
  let(:new_client2) { create(:client, nombre: 'cliente2') }

  before do
    new_client
    new_client2
  end

  it { is_expected.to have_many(:sales).dependent(:destroy) }

  context 'when user wants to search a client' do
    it 'finds the client' do
      termino = 'cliente1'

      expect(described_class.buscador(termino).count).to be >= 1
    end
  end

  context 'when user inputs half of the clients name' do
    it 'finds the client' do
      termino = 'clie'

      expect(described_class.buscador(termino).count).to be >= 1
    end
  end
end
