# frozen_string_literal: true

# == Schema Information
#
# Table name: sale_details
#
#  id         :bigint           not null, primary key
#  cantidad   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  product_id :bigint           not null
#  sale_id    :bigint           not null
#
# Indexes
#
#  index_sale_details_on_product_id  (product_id)
#  index_sale_details_on_sale_id     (sale_id)
#
# Foreign Keys
#
#  fk_rails_...  (product_id => products.id)
#  fk_rails_...  (sale_id => sales.id)
#
require 'rails_helper'

RSpec.describe SaleDetail, type: :model do
  it { is_expected.to belong_to(:product) }
  it { is_expected.to belong_to(:sale) }
end
