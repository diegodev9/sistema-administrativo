# frozen_string_literal: true

# == Schema Information
#
# Table name: profiles
#
#  id         :bigint           not null, primary key
#  apellido   :string
#  ciudad     :string
#  direccion  :string
#  estado     :string
#  foto       :string
#  nombre     :string
#  zip        :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :bigint
#
# Indexes
#
#  index_profiles_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
require 'rails_helper'

RSpec.describe Profile, type: :model do
  it { is_expected.to belong_to(:user) }
  it { is_expected.to have_one_attached(:profile_image) }
end
