# frozen_string_literal: true

# == Schema Information
#
# Table name: categories
#
#  id          :bigint           not null, primary key
#  nombre      :string
#  descripcion :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
require 'rails_helper'

RSpec.describe Category, type: :model do
  let(:new_product) { create(:product) }

  before do
    new_product
  end

  it { is_expected.to have_many(:products).dependent(:destroy) }
end
