# frozen_string_literal: true

require 'rails_helper'
require 'support/devise'

RSpec.describe 'Products', type: :request do
  let(:new_user) { create(:user) }

  before do
    sign_in new_user
  end

  describe 'GET /products' do
    it 'lists all products' do
      get '/products'
      expect(response).to have_http_status(:success)
    end
  end

  describe 'post /products' do
    let(:new_category) { create(:category) }
    let(:new_supplier) { create(:supplier) }
    let(:new_product) do
      { nombre: Faker::Commerce.product_name, descripcion: 'descripcion de producto',
        existencia: 100, precio: Faker::Commerce.price, category_id: new_category.id, supplier_id: new_supplier.id }
    end

    it 'creates a new product' do
      post '/products/', params: { product: new_product }, as: :json
      expect(response).to have_http_status(:created)
    end
  end

  describe 'patch /products' do
    let(:new_product) { create(:product) }

    it 'updates a product' do
      patch "/products/#{new_product.id}", params: { nombre: 'producto_nuevo' }, as: :json
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'delete /product' do
    let(:new_product) { create(:product) }

    it 'deletes a product' do
      delete "/products/#{new_product.id}", as: :json
      expect(response).to have_http_status(:no_content)
    end
  end

  describe '/buscar_producto/:termino' do
    let(:new_product1) { create(:product, nombre: 'producto1') }
    let(:new_product2) { create(:product, nombre: 'producto2') }

    before do
      new_product1
      new_product2
    end

    it 'finds all products that matches the term' do
      get '/buscar_producto/producto', params: { termino: 'producto' }, as: :json
      expect(Product.buscador('producto').count).to be >= 1
    end

    it 'finds the product' do
      get '/buscar_producto/producto1', params: { termino: 'producto1' }, as: :json
      expect(response.body).to include('producto1')
    end
  end
end
