# frozen_string_literal: true

require 'rails_helper'
require 'support/devise'

RSpec.describe 'Sales', type: :request do
  let(:new_user) { create(:user) }
  let(:new_sale1) do
    create(:sale, user_id: new_user.id, sale_details: [FactoryBot.build(:sale_detail)])
  end

  before do
    sign_in new_user
  end

  describe 'get /sales' do
    it 'lists all sales' do
      get '/sales'
      expect(response).to have_http_status(:success)
    end
  end

  describe 'get /sale/:id' do
    it 'shows the sale' do
      get '/sales/', params: { id: new_sale1.id }, as: :js
      expect(response).to have_http_status(:success)
    end
  end

  describe 'delete /sales/:id' do
    it 'deletes the sale' do
      delete "/sales/#{new_sale1.id}", params: { id: new_sale1.id }, as: :json
      expect(response).to have_http_status(:no_content)
    end
  end
end
