# frozen_string_literal: true

require 'rails_helper'
require 'support/devise'

RSpec.describe 'Categories', type: :request do
  let(:new_user) { create(:user) }

  before do
    sign_in new_user
  end

  describe 'get /categories' do
    it 'lists all categories' do
      get '/categories'
      expect(response).to have_http_status(:success)
    end
  end

  describe 'post /categories' do
    it 'creates a new category' do
      post '/categories/', params: { nombre: 'categoria_nueva', descripcion: 'categoria nueva' }, as: :json
      expect(response).to have_http_status(:created)
    end
  end

  describe 'patch /categories' do
    let(:new_category) { create(:category) }

    it 'updates a category' do
      patch "/categories/#{new_category.id}", params: { nombre: 'categoria nueva', descripcion: 'categoria nueva' }, as: :json
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'delete /categories' do
    let(:new_category) { create(:category) }

    it 'deletes a category' do
      delete "/categories/#{new_category.id}", as: :json
      expect(response).to have_http_status(:no_content)
    end
  end
end
