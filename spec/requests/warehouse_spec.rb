# frozen_string_literal: true

require 'rails_helper'
require 'support/devise'

RSpec.describe 'Warehouses', type: :request do
  let(:new_user) { create(:user) }

  before do
    sign_in new_user
  end

  describe 'get /warehouses' do
    it 'lists all warehouses' do
      get '/warehouses'
      expect(response).to have_http_status(:success)
    end
  end

  describe 'get /warehouses/new' do
    it 'creates a new warehouse and redirects to edit it' do
      get '/warehouses/new'
      expect(response).to have_http_status(:redirect)
    end
  end

  describe 'delete /warehouses/:id' do
    let(:new_product) { create(:product) }
    let(:new_warehouse) { create(:warehouse_record, supplier_id: new_product.supplier_id, product_id:  new_product.id, user_id: new_user.id) }
    let(:new_warehouse2) { create(:warehouse_record, supplier_id: new_product.supplier_id, product_id:  new_product.id, user_id: new_user.id) }

    it 'deletes a warehouse record' do
      delete "/warehouses/#{new_warehouse.id}", as: :json
      expect(response).to have_http_status(:no_content)
    end
  end
end
