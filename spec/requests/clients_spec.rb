# frozen_string_literal: true

require 'rails_helper'
require 'support/devise'

RSpec.describe 'Clients', type: :request do
  let(:new_user) { create(:user) }

  before do
    sign_in new_user
  end

  describe 'GET /clients' do
    it 'lists all clients' do
      get '/clients'
      expect(response).to have_http_status(:success)
    end
  end

  describe 'post /clients' do
    it 'creates a new client' do
      post '/clients/', params: { nombre: 'cliente_nuevo', direccion: 'cliente direccion', telefono: '0303456' }, as: :json
      expect(response).to have_http_status(:created)
    end
  end

  describe 'patch /clients' do
    let(:new_client) { create(:client) }

    it 'updates a client' do
      patch "/clients/#{new_client.id}", params: { nombre: 'cliente_nuevo', direccion: 'cliente direccion', telefono: '0303456' }, as: :json
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'delete /client' do
    let(:new_client) { create(:client) }

    it 'deletes a client' do
      delete "/clients/#{new_client.id}", as: :json
      expect(response).to have_http_status(:no_content)
    end
  end

  describe '/buscador_clientes/:termino' do
    let(:new_client1) { create(:client, nombre: 'cliente1') }
    let(:new_client2) { create(:client, nombre: 'cliente2') }

    before do
      new_client1
      new_client2
    end

    it 'finds all clients that matches the term' do
      get '/buscador_clientes/cliente', params: { termino: 'cliente' }, as: :json
      expect(Client.buscador('cliente').count).to be >= 1
    end

    it 'finds the client' do
      get '/buscador_clientes/cliente1', params: { termino: 'cliente1' }, as: :json
      expect(response.body).to include('cliente1')
    end
  end
end
