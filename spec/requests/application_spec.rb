# frozen_string_literal: true

require 'rails_helper'
require 'support/devise'

RSpec.describe 'Application', type: :request do
  describe 'user is not logged in' do
    it 'redirect to sign_in if user is not logged in' do
      expect(get('/')).to redirect_to('/users/sign_in')
    end
  end

  describe 'user is logged in' do
    let(:new_user) { create(:user) }

    before do
      sign_in new_user
      get '/'
    end

    it 'redirect to root_path if user is logged in' do
      expect(response).to have_http_status(:success)
    end
  end
end
