# frozen_string_literal: true

require 'rails_helper'
require 'support/devise'

RSpec.describe 'Suppliers', type: :request do
  let(:new_user) { create(:user) }

  before do
    sign_in new_user
  end

  describe 'get /suppliers' do
    it 'lists all suppliers' do
      get '/'
      expect(response).to have_http_status(:success)
    end
  end

  describe 'post /suppliers' do
    let(:new_supplier) do
      {
        nombre: Faker::Commerce.vendor,
        direccion: Faker::Address.full_address,
        telefono: Faker::PhoneNumber.phone_number_with_country_code
      }
    end

    it 'create a supplier' do
      post '/suppliers', params: { supplier: new_supplier }, as: :json
      expect(response).to have_http_status(:created)
    end
  end

  describe 'patch /suppliers' do
    let(:new_supplier) { create(:supplier) }

    it 'updates a supplier' do
      patch "/suppliers/#{new_supplier.id}", params: { direccion: 'prueba' }, as: :json
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'delete /suppliers/:id' do
    let(:new_supplier) { create(:supplier) }

    it 'deletes a supplier' do
      delete "/suppliers/#{new_supplier.id}", as: :json
      expect(response).to have_http_status(:no_content)
    end
  end

  describe '/buscador_proveedores/:termino' do
    let(:new_supplier1) { create(:supplier, nombre: 'proveedor1') }
    let(:new_supplier2) { create(:supplier, nombre: 'proveedor2') }

    before do
      new_supplier1
      new_supplier2
    end

    it 'finds all suppliers that matches the term' do
      get '/buscar_producto/producto', params: { termino: 'proveed' }, as: :json
      expect(Supplier.buscador('proveed').count).to be >= 1
    end

    it 'finds the supplier' do
      get '/buscador_proveedores/proveedor1', params: { termino: 'proveedor1' }, as: :json
      expect(response.body).to include('proveedor1')
    end
  end
end
