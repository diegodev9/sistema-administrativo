# Sistema Administrativo

Sistema administrativo creado con ruby 3.0.4, rails 6.1.6.1 y postgresql

Que incluye:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...

Instalación entorno desarrollo:

* Clonar repositorio
* Ejecutar bundle install
* Ejecutar rails assets:precompile

Instalación en produccion:

* Tener un servidor con: linux/windows, ruby, rails, postgresql, yarn, nvm, nginx, passenger
* Seguir la guia de: https://www.phusionpassenger.com/library/walkthroughs/deploy/ruby/
