# frozen_string_literal: true

# sales controller
class SalesController < ApplicationController
  before_action :set_sale, only: %i[show edit destroy add_item add_client]

  def index
    # @sales = Sale.all
    @sales = Sale.includes(%i[user client]) # recomendacion de bullet gem para prevenir query n+1
  end

  def new
    @sale = current_user.sales.create(importe: 0.0)
    redirect_to edit_sale_path(@sale)
  end

  def show; end

  def edit
    @products_sale = @sale.sale_details
  end

  def destroy
    ActiveRecord::Base.transaction do
      @sale.sale_details.map do |detail|
        prod_vendido = Product.find(detail.product_id)
        prod_vendido.existencia += detail.cantidad
        ActiveRecord::Rollback unless prod_vendido.save
      end

      ActiveRecord::Rollback unless @sale.destroy
    end

    respond_to do |format|
      format.html { redirect_to sales_url, notice: 'La venta ha sido eliminada' }
      format.json { head :no_content }
    end
  end

  # rubocop:disable Metrics/AbcSize, Metrics/MethodLength
  def add_item
    producto = Product.find(params[:producto_id])
    cantidad = params[:cantidad].nil? ? 1 : params[:cantidad].to_i
    importe_producto = producto.precio * cantidad
    @sale_detail = @sale.sale_details.build(product: producto, cantidad: cantidad)
    importe_antes_de_registro = @sale.importe
    importe_despues_de_registro = importe_antes_de_registro + importe_producto
    @sale.importe = importe_despues_de_registro
    existencia_antes_venta = producto.existencia
    result = { product_id: @sale_detail.product_id, precio_producto: producto.precio,
               nombre_producto: @sale_detail.product&.nombre, cantidad: @sale_detail.cantidad,
               importe_item: producto.precio * cantidad, importe_venta: importe_despues_de_registro }
    producto.existencia = existencia_antes_venta - cantidad
    respond_to do |format|
      if @sale.save && producto.save
        format.json { render json: result }
      else
        format.json { render json: @sale.errors.full_messages, status: :unprocessable_entity }
      end
    end
  end
  # rubocop:enable Metrics/AbcSize, Metrics/MethodLength

  def add_client
    cliente = Client.find(params[:cliente_id])

    if cliente.present?
      @sale.client = cliente
      if @sale.valid?
        result = { nombre_cliente: @sale.client.try(:nombre) }

        respond_to do |format|
          if @sale.save
            format.json { render json: result }
          else
            format.json { render json: @sale.errors.full_messages, status: :unprocessable_entity }
          end
        end
      end
    else
      render json: { message: 'El cliente no se encontró' }, status: :not_found
    end
  end

  private

  def set_sale
    @sale = Sale.find(params[:id])
  end
end
