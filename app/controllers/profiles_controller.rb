# frozen_string_literal: true

# Profile controller
class ProfilesController < ApplicationController
  before_action :set_profile
  def show; end

  def edit; end

  def update
    respond_to do |format|
      if @profile.update(profile_params)
        format.json { head :no_content }
        format.html { redirect_to @profile, notice: 'Perfil actualizado' }
      else
        format.json { render json: @profile.errors.full_messages, status: :unprocessable_entity }
        format.html { render :edit }
      end
    end
  end

  private

  def set_profile
    @profile = current_user.profile ||= Profile.create
  end

  def profile_params
    params.require(:profile).permit(:foto, :nombre, :apellido, :direccion,
                                    :ciudad, :estado, :zip, :user_id, :profile_image)
  end
end
