# frozen_string_literal: true

# application controller
class ApplicationController < ActionController::Base
  before_action :authenticate_user!
  layout :layout_by_resource

  def not_found_method
    redirect_to root_path
  end

  private

  def layout_by_resource
    if user_signed_in?
      'application'
    else
      'devise'
    end
  end
end
