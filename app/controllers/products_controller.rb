# frozen_string_literal: true

# products controller
class ProductsController < ApplicationController
  before_action :set_product, only: %i[show edit update destroy]
  before_action :set_categories, only: %i[new edit create update]
  before_action :set_suppliers, only: %i[new edit create update]

  def index
    @products = Product.all
  end

  def new
    @product = Product.new
  end

  def edit; end

  def show; end

  def create
    @product = Product.new(product_params)

    respond_to do |format|
      if @product.save
        format.json { head :created }
        format.js
      else
        format.json { render json: @product.errors.full_messages, status: :unprocessable_entity }
        format.js { render :new }
      end
    end
  end

  def update
    respond_to do |format|
      if @product.update(product_params)
        format.json { head :ok }
        format.js
      else
        format.json { render json: @product.errors.full_messages, status: :unprocessable_entity }
        format.js { render :edit }
      end
    end
  end

  def destroy
    @product.image.purge
    @product.destroy
    respond_to do |format|
      format.json { head :no_content }
      format.js
    end
  end

  def buscador
    @results = Product.buscador(params[:termino]).map do |producto|
      { id: producto.id, nombre: producto.nombre, existencia: producto.existencia }
    end

    respond_to do |format|
      format.json { render json: @results }
    end
  end

  private

  def set_product
    @product = Product.find(params[:id])
  end

  def set_categories
    @categories = Category.all.map { |category| [category.nombre, category.id] }.sort
  end

  def set_suppliers
    @suppliers = Supplier.all.map { |supplier| [supplier.nombre, supplier.id] }.sort
  end

  def product_params
    params.require(:product).permit(:nombre, :descripcion, :existencia, :precio, :category_id, :supplier_id, :image)
  end
end
