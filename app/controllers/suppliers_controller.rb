# frozen_string_literal: true

# suppliers controller
class SuppliersController < ApplicationController
  before_action :set_supplier, only: %i[edit update destroy]

  def index
    @suppliers = Supplier.all
  end

  def new
    @supplier = Supplier.new
  end

  def edit; end

  def create
    @supplier = Supplier.new(supplier_params)

    respond_to do |format|
      if @supplier.save
        format.json { head :created }
        format.js
      else
        format.json { render json: @supplier.errors.full_messages, status: :unprocessable_entity }
        format.js { render :new }
      end
    end
  end

  def update
    respond_to do |format|
      if @supplier.update(supplier_params)
        format.json { head :ok }
        format.js
      else
        format.json { render json: @supplier.errors.full_messages, status: :unprocessable_entity }
        format.js { render :edit }
      end
    end
  end

  def destroy
    @supplier.destroy
    respond_to do |format|
      format.json { head :no_content }
      format.js
    end
  end

  def buscador
    @resultados = Supplier.buscador(params[:termino]).map do |proveedor|
      {
        id: proveedor.id,
        nombre_proveedor: proveedor.nombre
      }
    end

    respond_to do |format|
      format.json { render json: @resultados }
    end
  end

  private

  def set_supplier
    @supplier = Supplier.find(params[:id])
  end

  def supplier_params
    params.require(:supplier).permit(:nombre, :direccion, :telefono)
  end
end
