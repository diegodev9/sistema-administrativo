$(document).on('turbolinks:load', function () {
    //SB-ADMIN-2
    (function ($) {
        "use strict"; // Start of use strict

        // Toggle the side navigation
        $("#sidebarToggle, #sidebarToggleTop").on('click', function (e) {
            $("body").toggleClass("sidebar-toggled");
            $(".sidebar").toggleClass("toggled");
            if ($(".sidebar").hasClass("toggled")) {
                $('.sidebar .collapse').collapse('hide');
            }

        });

        // Close any open menu accordions when window is resized below 768px
        $(window).resize(function () {
            if ($(window).width() < 768) {
                $('.sidebar .collapse').collapse('hide');
            }


            // Toggle the side navigation when window is resized below 480px
            if ($(window).width() < 480 && !$(".sidebar").hasClass("toggled")) {
                $("body").addClass("sidebar-toggled");
                $(".sidebar").addClass("toggled");
                $('.sidebar .collapse').collapse('hide');
            }

        });

        // Prevent the content wrapper from scrolling when the fixed side navigation hovered over
        $('body.fixed-nav .sidebar').on('mousewheel DOMMouseScroll wheel', function (e) {
            if ($(window).width() > 768) {
                var e0 = e.originalEvent,
                    delta = e0.wheelDelta || -e0.detail;
                this.scrollTop += (delta < 0 ? 1 : -1) * 30;
                e.preventDefault();
            }
        });

        // Scroll to top button appear
        $(document).on('scroll', function () {
            var scrollDistance = $(this).scrollTop();
            if (scrollDistance > 100) {
                $('.scroll-to-top').fadeIn();
            } else {
                $('.scroll-to-top').fadeOut();
            }
        });

        // Smooth scrolling using jQuery easing
        $(document).on('click', 'a.scroll-to-top', function (e) {
            var $anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: ($($anchor.attr('href')).offset().top)
            }, 1000, 'easeInOutExpo');
            e.preventDefault();
        });

    })(jQuery); // End of use strict
    //SB-ADMIN-2

    //buscador producto
    $("#buscador_productos").keyup(function (event) {
        let termino = $(this).val();
        let id_modelo = $(this).data("model");
        let tipo_modelo = $(this).data("tipo");

        if (termino.length === 0) {
            $("#tabla_buscador tbody").empty()
        } else {
            let request_url = getRootUrl() + "/buscar_producto/" + termino;
            $.get(request_url, function (data, status) {
                let newRowContent;
                if (data.length > 0) {
                    $("#tabla_buscador tbody").empty();
                    for (let x in data) {
                        let nombre_producto = data[x].nombre;
                        let existencia = data[x].existencia;
                        let id_producto = data[x].id;

                        newRowContent = `<tr>
                            <td>${nombre_producto}</td>
                            <td>${existencia}</td>
                            <td><button type="button" class="btn btn-primary" onclick="getProductInfo(${id_producto},${id_modelo},'${tipo_modelo}')">Agregar</button></td>
                            </tr>`;
                        $("#tabla_buscador tbody").append(newRowContent);
                    }
                }
            });
        }
    });
    $("#buscador_clientes").keyup(function (event) {
        let termino = $(this).val();
        let id_venta = $(this).data("venta");
        if (termino.length == 0) {
            $("#tabla_buscador_clientes tbody").empty();
        } else {
            let request_url = getRootUrl() + "/buscador_clientes/" + termino;
            $.get(request_url, function (data, status) {
                if (data.length > 0) {
                    $("#tabla_buscador_clientes tbody").empty();
                    for (let x in data) {
                        let nombre = data[x].nombre_cliente;
                        let id_cliente = data[x].id;
                        let rowContent = `<tr>
                                <td>${nombre}</td>
                                <td>
                                <button class="btn btn-primary" onclick="getClientInfo(${id_cliente}, ${id_venta});">Agregar</button>
                                </td>
                            </tr>`;
                        $("#tabla_buscador_clientes tbody").append(rowContent);
                    }
                }
            });
        }
    });
    $("#buscador_proveedores").keyup(function (event) {
        let termino = $(this).val();
        let id_registro_almacen = $(this).data("warehouse");
        if (termino.length == 0) {
            $("#tabla_buscador_proveedores tbody").empty();
        } else {
            let request_url = getRootUrl() + "/buscador_proveedores/" + termino;
            $.get(request_url, function (data, status) {
                if (data.length > 0) {
                    $("#tabla_buscador_proveedores tbody").empty();
                    for (let x in data) {
                        let nombre = data[x].nombre_proveedor;
                        let id_proveedor = data[x].id;
                        let rowContent = `<tr>
                                <td>${nombre}</td>
                                <td>
                                <button class="btn btn-primary" onclick="getSupplierInfo(${id_proveedor}, ${id_registro_almacen});">Agregar</button>
                                </td>
                            </tr>`;
                        $("#tabla_buscador_proveedores tbody").append(rowContent);
                    }
                }
            });
        }
    });
});

window.getProductInfo = function (id_producto, id_modelo, tipo_modelo) {
    //let id_producto = id_producto;
    //let id_modelo = id_modelo;
    //let tipo_modelo = tipo_modelo;

    switch (tipo_modelo) {
        case 'sales':
            agregarItemVenta(id_producto, id_modelo);
            break;
        case 'warehouse':
            agregarProductoAlmacen(id_producto, id_modelo);
            break;
    }
};

window.getClientInfo = function (id_cliente, id_venta) {
    let request_url = getRootUrl() + "/add_cliente_venta/";
    let info = {cliente_id: id_cliente, id: id_venta};
    $.ajax({
        url: request_url,
        type: 'POST',
        data: JSON.stringify(info),
        contentType: 'application/json; charset=utf-8',
        success: function (result) {
            if (result != null) {
                $("#buscar_cliente").modal('hide');
                $("body").removeClass('modal-open');
                $(".modal-backdrop").remove();
                let nombre_cliente = result.nombre_cliente;
                $("#cliente_venta").html("Cliente: " + nombre_cliente);
            }
        }
    });
};

window.getSupplierInfo = function (id_proveedor, id_registro_almacen) {
    let request_url = getRootUrl() + "/add_proveedor_entrada/";
    let info = {proveedor_id: id_proveedor, id: id_registro_almacen};
    $.ajax({
        url: request_url,
        type: 'POST',
        data: JSON.stringify(info),
        contentType: 'application/json; charset=utf-8',
        success: function (result) {
            if (result != null) {
                $("#buscar_proveedor").modal('hide');
                $("body").removeClass('modal-open');
                $(".modal-backdrop").remove();
                let nombre_proveedor = result.nombre_proveedor;
                $("#proveedor-entrada").html("Proveedor: " + nombre_proveedor);
            }
        }
    });
};

window.agregarItemVenta = function (id_producto, id_venta) {
    let cantidad_inicial = $("#cantidad_producto").val();
    let request_url = getRootUrl() + "/add_item_venta/";
    let info = {producto_id: id_producto, id: id_venta, cantidad: cantidad_inicial}
    $.ajax({
        url: request_url,
        type: 'POST',
        data: JSON.stringify(info),
        contentType: 'application/json; charset=utf-8',
        success: function (result) {
            if (result != null) {
                $("#buscar_producto").modal('hide');
                $("body").removeClass('modal-open');
                $(".modal-backdrop").remove();
                let cantidad = result.cantidad;
                let precio = result.precio_producto;
                let importe_item = result.importe_item;
                let importe_venta = result.importe_venta;
                let nombre_prod = result.nombre_producto;
                let product_id = result.product_id;
                let newRowContent = `<tr>
                                        <td>${nombre_prod}</td>
                                        <td>${precio}</td>
                                        <td>${cantidad}</td>
                                        <td>${importe_item}</td>
                                    </tr>`;
                $("#tabla_ventas tbody").html('').append(newRowContent);
                $("#importe_venta_lbl").text("Importe: $" + importe_venta);
            }
        }
    });
};

window.agregarProductoAlmacen = function (id_producto, id_entrada_almacen) {
    let cantidad_inicial = $("#cantidad_producto").val();
    let request_url = getRootUrl() + '/add_item_almacen/';
    let info = {producto_id: id_producto, id: id_entrada_almacen, cantidad: cantidad_inicial};
    $.ajax({
        url: request_url,
        type: 'POST',
        data: JSON.stringify(info),
        contentType: 'application/json',
        success: function (result) {
            console.log("resultado: " + result);
            if (result != null) {
                $("#buscar_producto").modal('hide');
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();

                let id_almacen = result.id;
                let cantidad = result.cantidad;
                let producto = result.producto;

                let registro_almacen = `<tr>
                                        <td>${id_almacen}</td>
                                        <td>${producto}</td>
                                        <td>${cantidad}</td>
                                        </tr>`;
                $("#tabla_entradas_almacen tbody").html('').append(registro_almacen);
            }
        }
    });
};

//obtener root url
window.getRootUrl = function () {
    return window.location.origin;
};