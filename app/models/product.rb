# frozen_string_literal: true

# == Schema Information
#
# Table name: products
#
#  id          :bigint           not null, primary key
#  descripcion :string
#  existencia  :integer
#  nombre      :string
#  precio      :decimal(, )
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  category_id :bigint           not null
#  supplier_id :bigint           not null
#
# Indexes
#
#  index_products_on_category_id  (category_id)
#  index_products_on_supplier_id  (supplier_id)
#
# Foreign Keys
#
#  fk_rails_...  (category_id => categories.id)
#  fk_rails_...  (supplier_id => suppliers.id)
#
class Product < ApplicationRecord
  belongs_to :category
  belongs_to :supplier
  has_many :warehouse_records, dependent: :destroy
  has_one_attached :image, dependent: :destroy

  # falta agregar validacion de imagen

  scope :buscador, ->(termino) { where('nombre LIKE ?', "%#{termino}%") }
end
