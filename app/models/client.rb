# frozen_string_literal: true

# == Schema Information
#
# Table name: clients
#
#  id         :bigint           not null, primary key
#  direccion  :string
#  nombre     :string
#  telefono   :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Client < ApplicationRecord
  has_many :sales, dependent: :destroy

  scope :buscador, ->(termino) { where('nombre LIKE ?', "%#{termino}%") }
end
