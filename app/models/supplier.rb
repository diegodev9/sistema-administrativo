# frozen_string_literal: true

# == Schema Information
#
# Table name: suppliers
#
#  id         :bigint           not null, primary key
#  direccion  :string
#  nombre     :string
#  telefono   :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Supplier < ApplicationRecord
  has_many :products, dependent: :destroy
  has_many :warehouse_records, dependent: :destroy

  scope :buscador, ->(termino) { where('nombre LIKE ?', "%#{termino}%") }
end
