# frozen_string_literal: true

# == Schema Information
#
# Table name: warehouse_details
#
#  id                  :bigint           not null, primary key
#  cantidad            :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  product_id          :bigint
#  warehouse_record_id :bigint
#
# Indexes
#
#  index_warehouse_details_on_product_id           (product_id)
#  index_warehouse_details_on_warehouse_record_id  (warehouse_record_id)
#
# Foreign Keys
#
#  fk_rails_...  (product_id => products.id)
#  fk_rails_...  (warehouse_record_id => warehouse_records.id)
#
class WarehouseDetail < ApplicationRecord
  belongs_to :product
  belongs_to :warehouse_record
end
