# frozen_string_literal: true

class RemoveNullConditionOnWarehouseRecords < ActiveRecord::Migration[6.1]
  def change
    change_column_null :warehouse_records, :supplier_id, true
    change_column_null :warehouse_records, :products_id, true
  end
end
