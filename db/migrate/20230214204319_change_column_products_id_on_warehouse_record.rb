class ChangeColumnProductsIdOnWarehouseRecord < ActiveRecord::Migration[6.1]
  def change
    remove_reference :warehouse_records, :products
    add_reference :warehouse_records, :product, foreign_key: true
  end
end
