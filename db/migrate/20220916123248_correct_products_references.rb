# frozen_string_literal: true

# correct products references
# rubocop:disable Rails/NotNullColumn
class CorrectProductsReferences < ActiveRecord::Migration[6.1]
  def change
    remove_reference :products, :categories
    remove_reference :products, :suppliers

    add_reference :products, :category, foreign_key: true, null: false
    add_reference :products, :supplier, foreign_key: true, null: false
  end
end
# rubocop:enable Rails/NotNullColumn
